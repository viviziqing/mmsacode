"""
ref:1. CH-SIMS: A Chinese Multimodal Sentiment Analysis Dataset with Fine-grained Annotations of Modality https://github.com/A2Zadeh/TensorFusionNetwork
    2. Multimodal Transformer for Unaligned Multimodal Language Sequences https://github.com/yaohungt/Multimodal-Transformer
"""
from re import S
import torch
import torch.nn as nn
import torch.nn.functional as F
from TRANSIM import TRANSIM

__all__ = ['MMSA']

class SubNet(nn.Module):
    

    def __init__(self, in_size, hidden_size, dropout):
        
        super(SubNet, self).__init__()
        self.norm = nn.BatchNorm1d(in_size)
        self.drop = nn.Dropout(p=dropout)
        self.linear_1 = nn.Linear(in_size, hidden_size)
        self.linear_2 = nn.Linear(hidden_size, hidden_size)
        self.linear_3 = nn.Linear(hidden_size, hidden_size)
        self.tanh=nn.Tanh()

    def forward(self, x):
        
        normed = self.norm(x)
        dropped = self.drop(normed)
        y_1 = self.tanh(self.linear_1(dropped))
        y_2 = self.tanh(self.linear_2(y_1))
        y_3 = self.tanh(self.linear_3(y_2))

        return y_3


class TextSubNet(nn.Module):
    

    def __init__(self, in_size, hidden_size, out_size, num_layers=1, dropout=0.2, bidirectional=False):
        
        super(TextSubNet, self).__init__()
        if num_layers == 1:
            dropout = 0.0
        self.rnn = nn.LSTM(in_size, hidden_size, num_layers=num_layers, dropout=dropout, bidirectional=bidirectional, batch_first=True)
        self.dropout = nn.Dropout(dropout)
        self.norm = nn.BatchNorm1d(hidden_size)
        self.linear_1 = nn.Linear(hidden_size, out_size)
        self.tanh=nn.Tanh()

    def forward(self, x):
        
        _, final_states = self.rnn(x)
        h = self.dropout(final_states[0].squeeze())
        normed = self.norm(h)
        y_1 = self.tanh(self.linear_1(normed))
        return y_1


class MMSA(nn.Module):
    
    def __init__(self, args):
        super(MMSA, self).__init__()
        self.text_in, self.audio_in, self.video_in = args.feature_dims
        self.text_hidden, self.audio_hidden, self.video_hidden = args.hidden_dims
        self.text_out = self.text_hidden

        self.audio_prob, self.video_prob, self.text_prob = args.dropouts
        self.post_text_prob, self.post_audio_prob, self.post_video_prob, self.post_fusion_prob = args.post_dropouts

        self.post_fusion_dim = args.post_fusion_dim
        self.post_text_dim = args.post_text_dim
        self.post_audio_dim = args.post_audio_dim
        self.post_video_dim = args.post_video_dim
        self.orig_d_l, self.orig_d_a, self.orig_d_v = args.hidden_dims
        self.t=torch.tensor(1.0,requires_grad=True)
        self.a=torch.tensor(1.0,requires_grad=True)
        self.v=torch.tensor(1.0,requires_grad=True)
        self.f=torch.tensor(1.0,requires_grad=True)
        self.fm=torch.tensor(1.0,requires_grad=True)

        
        self.neednorm=args.neednorm
        output_dim = args.num_classes if args.train_mode == "classification" else 1
        self.ffdropout=args.ffdropout
        self.audio_subnet = SubNet(self.audio_in, self.audio_hidden, self.audio_prob)
        self.video_subnet = SubNet(self.video_in, self.video_hidden, self.video_prob)
        self.text_subnet = TextSubNet(self.text_in, self.text_hidden, self.text_out, dropout=self.text_prob)
        self.mfneednorm = args.mfneednorm
        self.fusion=TRANSIM(args)

        
        self.post_text_dropout = nn.Dropout(p=self.post_text_prob)
        self.tnorm1 = nn.BatchNorm1d(self.text_out)
        self.post_text_layer_1 = nn.Linear(self.text_out, self.post_text_dim)
        self.tnorm2 = nn.BatchNorm1d(self.post_text_dim)
        self.post_text_layer_2 = nn.Linear(self.post_text_dim, self.post_text_dim)
        self.tnorm3 = nn.BatchNorm1d(self.post_text_dim)
        self.post_text_layer_3 = nn.Linear(self.post_text_dim, output_dim)

 
        self.post_audio_dropout = nn.Dropout(p=self.post_audio_prob)
        self.anorm1 = nn.BatchNorm1d(self.audio_hidden)
        self.post_audio_layer_1 = nn.Linear(self.audio_hidden, self.post_audio_dim)
        self.anorm2 = nn.BatchNorm1d(self.post_audio_dim)
        self.post_audio_layer_2 = nn.Linear(self.post_audio_dim, self.post_audio_dim)
        self.anorm3 = nn.BatchNorm1d(self.post_audio_dim)
        self.post_audio_layer_3 = nn.Linear(self.post_audio_dim, output_dim)

        self.post_video_dropout = nn.Dropout(p=self.post_video_prob)
        self.vnorm1 = nn.BatchNorm1d(self.video_hidden)
        self.post_video_layer_1 = nn.Linear(self.video_hidden, self.post_video_dim)
        self.vnorm2 = nn.BatchNorm1d(self.post_video_dim)
        self.post_video_layer_2 = nn.Linear(self.post_video_dim, self.post_video_dim)
        self.vnorm3 = nn.BatchNorm1d(self.post_video_dim)
        self.post_video_layer_3 = nn.Linear(self.post_video_dim, output_dim)

        self.fnorm1 = nn.BatchNorm1d(self.post_audio_dim+self.post_text_dim+self.post_video_dim+int(self.orig_d_l/4))
        self.ffneeddropout=args.ffneeddropout
        self.ff_dropout = nn.Dropout(p=self.ffdropout)
        self.post_fusion_layer_1=nn.Linear(self.post_audio_dim+self.post_text_dim+self.post_video_dim+int(self.orig_d_l/4),self.post_fusion_dim)
        self.fnorm2 = nn.BatchNorm1d(self.post_fusion_dim)
        self.post_fusion_layer_21=nn.Linear(self.post_fusion_dim,self.post_fusion_dim)
        self.fnorm3 = nn.BatchNorm1d(self.post_fusion_dim)
        self.post_fusion_layer_22=nn.Linear(self.post_fusion_dim,self.post_fusion_dim)
        self.fnorm4 = nn.BatchNorm1d(self.post_fusion_dim)
        self.post_fusion_layer_2=nn.Linear(self.post_fusion_dim,output_dim)

        self.ofnorm1=nn.BatchNorm1d(5*output_dim)
        self.post_fusion_layer_3=nn.Linear(5*output_dim,5*output_dim)
        self.ofnorm2=nn.BatchNorm1d(5*output_dim)
        self.post_fusion_layer_4=nn.Linear(5*output_dim,output_dim)
        
        self.tanh=nn.Tanh()


    def forward(self, text_x, audio_x, video_x):
        
        audio_x = audio_x.squeeze(1)
        video_x = video_x.squeeze(1)
        

        audio_h = self.audio_subnet(audio_x)
        video_h = self.video_subnet(video_x)
        text_h = self.text_subnet(text_x)
        

        text_h = self.tnorm1(text_h)
        x_t = self.post_text_dropout(text_h)
        x_t = self.tanh(self.post_text_layer_1(x_t))
        x_t1 = self.tanh(self.post_text_layer_2(x_t))
        output_text =self.post_text_layer_3(x_t1)
        
        
        audio_h = self.anorm1(audio_h)
        x_a = self.post_audio_dropout(audio_h)
        x_a = self.tanh(self.post_audio_layer_1(x_a))
        x_a1 = self.tanh(self.post_audio_layer_2(x_a))
        output_audio = self.post_audio_layer_3(x_a1)
        
        video_h = self.vnorm1(video_h)
        x_v = self.post_video_dropout(video_h)
        x_v = self.tanh(self.post_video_layer_1(x_v))
        x_v1 = self.tanh(self.post_video_layer_2(x_v))
        output_video = self.post_video_layer_3(x_v1)

        text_h1=text_h.unsqueeze(1)
        text_h1=text_h1.transpose(1,2)
        audio_h1=audio_h.unsqueeze(1)
        audio_h1=audio_h1.transpose(1,2)
        video_h1=video_h.unsqueeze(1)
        video_h1=video_h1.transpose(1,2)
        
        
        



        output_fusion=self.fusion(text_h1,audio_h1,video_h1)['M']    
        output_fusionx=self.fusion(text_h1,audio_h1,video_h1)['Feature_t']

        outputfusion=torch.cat([output_fusionx,x_t1],dim=1)
        outputfusion=torch.cat([outputfusion,x_a1],dim=1)
        outputfusion=torch.cat([outputfusion,x_v1],dim=1)
        if (self.mfneednorm):
            outputfusion = self.fnorm1(outputfusion)
        if(self.ffneeddropout):
            outputfusion = self.ff_dropout(outputfusion)
        output_fusionm=self.tanh(self.post_fusion_layer_1(outputfusion))
        output_fusionm=self.tanh(self.post_fusion_layer_21(output_fusionm))
        output_fusionm=self.tanh(self.post_fusion_layer_22(output_fusionm))
        output_fusionm=self.post_fusion_layer_2(output_fusionm)
        
        output_fusionm= (self.t*output_text+self.a*output_audio+self.v*output_video+self.f*output_fusion+self.fm*output_fusionm)

        

        res = {
            'Feature_t': text_h ,
            'Feature_a': audio_h,
            'Feature_v': video_h,
            'M': output_fusionm,
            'T': output_text,
            'A': output_audio,
            'V': output_video
        }
        return res