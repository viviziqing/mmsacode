"""
ref:1. CH-SIMS: A Chinese Multimodal Sentiment Analysis Dataset with Fine-grained Annotations of Modality https://github.com/A2Zadeh/TensorFusionNetwork
    2. Multimodal Transformer for Unaligned Multimodal Language Sequences https://github.com/yaohungt/Multimodal-Transformer
"""
import os
import sys
import collections

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd.function import Function

from transformer import TransformerEncoder
from BertTextEncoder import BertTextEncoder

__all__ = ['TRANSIM']

class TRANSIM(nn.Module):
    def __init__(self, args):
        super(TRANSIM, self).__init__()
        if args.use_bert:
            self.text_model = BertTextEncoder(language=args.language, use_finetune=args.use_bert_finetune)
        self.use_bert = args.use_bert
        dst_feature_dims, nheads = args.dst_feature_dim_nheads
        self.orig_d_l, self.orig_d_a, self.orig_d_v = args.hidden_dims
        self.d_l = self.d_a = self.d_v = dst_feature_dims
        self.text_hidden, self.audio_hidden, self.video_hidden = args.hidden_dims
        self.text_out = self.text_hidden
        self.num_heads = nheads
        self.layers = args.nlevels
        self.wailayers = args.nlayers
        
        self.attn_dropout = args.attn_dropout
        self.attn_dropout_a = args.attn_dropout_a
        self.attn_dropout_v = args.attn_dropout_v
        self.relu_dropout = args.relu_dropout
        self.embed_dropout = args.embed_dropout
        self.res_dropout = args.res_dropout
        self.output_dropout = args.output_dropout
        self.text_dropout = args.text_dropout
        self.attn_mask = args.attn_mask
        self.text_hidden, self.audio_hidden, self.video_hidden = args.hidden_dims
        self.fneednorm=args.fneednorm
        self.fneeddrop=args.fneeddrop
        self.fdropout=args.fdropout
        combined_dim = self.d_l
        
        output_dim = args.num_classes if args.train_mode == "classification" else 1
        
        self.lineara1=nn.Linear(self.orig_d_a,self.orig_d_a)
        self.lineara2=nn.Linear(self.orig_d_a,self.orig_d_a)
        self.lineara3=nn.Linear(self.orig_d_a,self.orig_d_a)

        self.linearv1=nn.Linear(self.orig_d_v,self.orig_d_v)
        self.linearv2=nn.Linear(self.orig_d_v,self.orig_d_v)
        self.linearv3=nn.Linear(self.orig_d_v,self.orig_d_v)
        
        self.linearl1=nn.Linear(self.orig_d_l,self.orig_d_l)
        self.linearl2=nn.Linear(self.orig_d_l,self.orig_d_l)
        self.linearl3=nn.Linear(self.orig_d_l,self.orig_d_l)
        
        self.linenormv=nn.BatchNorm1d(self.video_hidden)
        self.linenorma=nn.BatchNorm1d(self.audio_hidden)
        self.linenorml=nn.BatchNorm1d(self.video_hidden)


        self.trans_l_with_a = self.get_network(self_type='la')
        self.trans_l_with_v = self.get_network(self_type='lv')
        self.trans_l_with_l = self.get_network(self_type='l')
    
        self.trans_l_mem = self.get_network(self_type='l_mem', layers=3)

        self.ounorm2=nn.BatchNorm1d(self.orig_d_l)
        self.proj1 = nn.Linear(self.orig_d_l, int(self.orig_d_l/4))
        self.proj2 = nn.Linear(int(self.orig_d_l/4),int(self.orig_d_l/4) )
        self.ounorm2=nn.BatchNorm1d(int(self.orig_d_l/4))
        self.out_layer = nn.Linear(int(self.orig_d_l/4), output_dim)

        self.normv=nn.BatchNorm1d(self.video_hidden)
        self.norma=nn.BatchNorm1d(self.audio_hidden)
        self.norml=nn.BatchNorm1d(self.video_hidden)
        self.tanh=nn.Tanh()
        self.fnorm=nn.BatchNorm1d(self.orig_d_l)
        self.f_dropout = nn.Dropout(p=self.fdropout)
        




        

        

    def get_network(self,self_type, layers=-1):
        if self_type =='l':
            embed_dim, attn_dropout ,hiddendim= self.d_l, self.attn_dropout,self.text_hidden
        elif self_type=='lv':
            embed_dim, attn_dropout,hiddendim= self.d_l, self.attn_dropout,self.video_hidden
        elif self_type =='la':
            embed_dim, attn_dropout,hiddendim= self.d_l, self.attn_dropout,self.audio_hidden

        elif self_type == 'l_mem':
            embed_dim, attn_dropout,hiddendim= self.d_l, self.attn_dropout,self.text_hidden

        else:
            raise ValueError("Unknown network type")
        
        return TransformerEncoder(embed_dim=embed_dim,
                                  num_heads=self.num_heads,
                                  hiddendim=hiddendim,
                                  layers=max(self.layers, layers),
                                  attn_dropout=attn_dropout,
                                  relu_dropout=self.relu_dropout,
                                  res_dropout=self.res_dropout,
                                  embed_dropout=self.embed_dropout,
                                  attn_mask=self.attn_mask)

    def forward(self, text, audio, video):
        if self.use_bert:
            text = self.text_model(text)
        
        x_l = F.dropout(text.transpose(1, 2), p=self.text_dropout, training=self.training)
        x_a = audio.transpose(1, 2)
        x_v = video.transpose(1, 2)

        proj_x_a = x_a.permute(2, 0, 1)*10
        proj_x_v = x_v.permute(2, 0, 1)
        proj_x_l = x_l.permute(2, 0, 1)

        ##val
        proj_x_v = proj_x_v.squeeze()
        proj_x_v = proj_x_v.transpose(0,1)
        proj_x_v=self.linenormv(self.linearv1(proj_x_v))
        proj_x_v = proj_x_v.transpose(0,1)
        proj_x_v = proj_x_v.unsqueeze(2) 
        h_l_with_vs= self.trans_l_with_v(proj_x_v,proj_x_l,proj_x_l)
        h_l_with_vs = h_l_with_vs.squeeze()
        h_l_with_vs = h_l_with_vs.transpose(0,1)
        h_l_with_vs = self.normv(h_l_with_vs)
        h_l_with_vs = h_l_with_vs.transpose(0,1)
        h_l_with_vs = h_l_with_vs.unsqueeze(2) 
        
        proj_x_a = proj_x_a.squeeze()
        proj_x_a = proj_x_a.transpose(0,1)
        proj_x_a=self.linenorma(self.lineara1(proj_x_a))
        proj_x_a = proj_x_a.transpose(0,1)
        proj_x_a = proj_x_a.unsqueeze(2) 
        h_l_with_as = self.trans_l_with_a(proj_x_a, h_l_with_vs, h_l_with_vs) 
        h_l_with_as = h_l_with_as.squeeze()
        h_l_with_as = h_l_with_as.transpose(0,1)
        h_l_with_as = self.norma(h_l_with_as)
        h_l_with_as = h_l_with_as.transpose(0,1)
        h_l_with_as = h_l_with_as.unsqueeze(2) 

        proj_x_l = proj_x_l.squeeze()
        proj_x_l = proj_x_l.transpose(0,1)
        proj_x_l=self.linenorml(self.linearl1(proj_x_l))
        proj_x_l = proj_x_l.transpose(0,1)
        proj_x_l = proj_x_l.unsqueeze(2) 
        h_l_with_ls = self.trans_l_with_l(proj_x_l, h_l_with_as, h_l_with_as)  
        h_l_with_ls = h_l_with_ls.squeeze()
        h_l_with_ls = h_l_with_ls.transpose(0,1)
        h_l_with_ls = self.norml(h_l_with_ls)
        h_l_with_ls = h_l_with_ls.transpose(0,1)
        h_l_with_ls = h_l_with_ls.unsqueeze(2) 

        proj_x_a = proj_x_a.squeeze()
        proj_x_a = proj_x_a.transpose(0,1)
        proj_x_a=self.linenorma(self.lineara2(proj_x_a))
        proj_x_a = proj_x_a.transpose(0,1)
        proj_x_a = proj_x_a.unsqueeze(2) 
        h_l_with_as = self.trans_l_with_a(proj_x_a, h_l_with_ls, h_l_with_ls) 
        h_l_with_as = h_l_with_as.squeeze()
        h_l_with_as = h_l_with_as.transpose(0,1)
        h_l_with_as = self.norma(h_l_with_as)
        h_l_with_as = h_l_with_as.transpose(0,1)
        h_l_with_as = h_l_with_as.unsqueeze(2) 

        proj_x_v = proj_x_v.squeeze()
        proj_x_v = proj_x_v.transpose(0,1)
        proj_x_v=self.linenormv(self.linearv2(proj_x_v))
        proj_x_v = proj_x_v.transpose(0,1)
        proj_x_v = proj_x_v.unsqueeze(2) 
        h_l_with_vs= self.trans_l_with_v(proj_x_v,h_l_with_as,h_l_with_as)
        h_l_with_vs = h_l_with_vs.squeeze()
        h_l_with_vs = h_l_with_vs.transpose(0,1)
        h_l_with_vs = self.normv(h_l_with_vs)
        h_l_with_vs = h_l_with_vs.transpose(0,1)
        h_l_with_vs = h_l_with_vs.unsqueeze(2) 

        proj_x_l = proj_x_l.squeeze()
        proj_x_l = proj_x_l.transpose(0,1)
        proj_x_l=self.linenorml(self.linearl2(proj_x_l))
        proj_x_l = proj_x_l.transpose(0,1)
        proj_x_l = proj_x_l.unsqueeze(2) 
        h_l_with_ls = self.trans_l_with_l(proj_x_l, h_l_with_vs, h_l_with_vs)    
        h_l_with_ls = h_l_with_ls.squeeze()
        h_l_with_ls = h_l_with_ls.transpose(0,1)
        h_l_with_ls = self.norml(h_l_with_ls)
        h_l_with_ls = h_l_with_ls.transpose(0,1)
        h_l_with_ls = h_l_with_ls.unsqueeze(2) 





        
        
        h_ls = self.trans_l_mem(h_l_with_ls,h_l_with_ls,h_l_with_ls)
        
        if type(h_ls) == tuple:
            h_ls = h_ls[0]
        last_h_l = last_hs = h_ls[-1]   
 

        h_ls=torch.squeeze(h_ls)
        h_ls=h_ls.transpose(1,0)
        
        if(self.fneednorm):
            h_ls=self.fnorm(h_ls)
        if(self.fneeddrop):
            h_ls=self.f_dropout(h_ls)

        last_hs_proj = self.tanh(self.proj2(F.dropout(self.tanh(self.proj1(h_ls)), p=self.output_dropout, training=self.training)))
        
        output =self.out_layer(last_hs_proj)
        res = {
            'Feature_t': last_hs_proj,

            'M': output
        }
        return res
